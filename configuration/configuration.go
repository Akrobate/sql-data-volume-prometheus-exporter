package configuration

import (
    "strings"
    "os"
    "log"
    "errors"
    "sync"
    "github.com/ilyakaznacheev/cleanenv"
)

var instance ConfigDatabase
var once sync.Once

type ConfigDatabase struct {
    Refresh_Interval_Seconds     int `yaml:"refresh_interval_seconds" env:"REFRESH_INTERVAL_SECONDS"`
    Refresh_Fields_At_Interval_Seconds     int `yaml:"refresh_fields_at_interval_seconds" env:"REFRESH_FIELDS_AT_INTERVAL_SECONDS"`
    Exporter_Port     int `yaml:"exporter_port" env:"EXPORTER_PORT"`
    MySql_Port     string `yaml:"mysql_port" env:"MYSQL_PORT"`
    MySql_Host     string `yaml:"mysql_host" env:"MYSQL_HOST"`
    MySql_Name     string `yaml:"mysql_name" env:"MYSQL_NAME"`
    MySql_User     string `yaml:"mysql_user" env:"MYSQL_USER"`
    MySql_Password string `yaml:"mysql_password" env:"MYSQL_PASSWORD"`
    Watch_date_fields   string   `yaml:"watch_date_fields" env:"WATCH_DATE_FIELDS"`
    Watch_date_fields_list   []string
}


func New() ConfigDatabase {
    var cfg ConfigDatabase
    if _, err := os.Stat("configuration.yml"); errors.Is(err, os.ErrNotExist) {
        if err := cleanenv.ReadEnv(&cfg); err != nil {
            log.Print(err)
        }
    } else {
        if err := cleanenv.ReadConfig("configuration.yml", &cfg); err != nil {
            log.Print(err)
        }
    }
    cfg.Watch_date_fields_list = strings.Split(cfg.Watch_date_fields, ",")
    return cfg
}


func GetInstance() ConfigDatabase {
    once.Do(func() {
        instance = New()
    })
    return instance
}
