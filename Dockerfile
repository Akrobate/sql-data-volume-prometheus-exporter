FROM golang:1.17
COPY ./ /sql-data-volume-prometheus-exporter
WORKDIR /sql-data-volume-prometheus-exporter
CMD go run .