package databaseCountService

import (
    "log"
    "sql-data-volume-prometheus-exporter/mysqlConnection"
    "sql-data-volume-prometheus-exporter/configuration"
    "database/sql"
    "time"
    "fmt"
)


type TableCount struct {
    Name string
    Count int
}

type TableFieldCount struct {
    Table_Name string
    Field_Name string
    Count int
}

type TableHasFields struct {
    Table_Name string
    Field_Name string
}

func getAllTables() [] string {
    var table_name string
    var table_list []string
    db := mysqlConnection.GetConnection()
    rows, err := db.Query("SHOW TABLES")
    if err != nil {
        log.Fatal(err)
    }
    for rows.Next() {
        err := rows.Scan(&table_name)
        if err != nil {
            log.Fatal(err)
        }
        table_list = append(table_list, table_name)
    }
    return table_list
}


func GetTableCounts() []TableCount {

    var result []TableCount

    table_list := getAllTables()
    db := mysqlConnection.GetConnection()
    for _, table_name := range table_list {
        var table_count int
        rows, err := db.Query("SELECT count(*) FROM " + table_name)
        if err != nil {
            log.Fatal(err)
        }
        for rows.Next() {
            if err := rows.Scan(&table_count); err != nil {
                log.Fatal(err)
            }
        }
        result = append(result, TableCount{
            Name: table_name,
            Count: table_count,
        })
    }

    return result
}


func GetTableHasFields() []TableHasFields {

    var result []TableHasFields
    configuration := configuration.GetInstance()

    table_list := getAllTables()
    db := mysqlConnection.GetConnection()
    for _, table_name := range table_list {
        
        rows, err := db.Query("DESCRIBE " + table_name)
        if err != nil {
            log.Fatal(err)
        }

        for rows.Next() {
            var field_name sql.NullString
            var type_name sql.NullString
            var field_1 sql.NullString
            var field_2 sql.NullString
            var field_3 sql.NullString
            var field_4 sql.NullString

            if err := rows.Scan(&field_name, &type_name, &field_1, &field_2, &field_3, &field_4); err != nil {
                log.Fatal(err)
            }

            if (field_name.Valid && type_name.String == "datetime") {
                if (stringInSlice(field_name.String, configuration.Watch_date_fields_list)) {
                    result = append(result, TableHasFields{
                        Table_Name: table_name,
                        Field_Name: field_name.String,
                    })
                }
            }
        }
    }
    return result
}


func stringInSlice(a string, list []string) bool {
    for _, b := range list {
        if b == a {
            return true
        }
    }
    return false
}


func GetTableDateFieldAtCountList(from_date time.Time, to_date time.Time) []TableFieldCount {
    var result []TableFieldCount
    table_has_fields_list := GetTableHasFields()
    for _, table_has_fields := range table_has_fields_list {
        table_count := getDateFilteredFieldCount(table_has_fields.Table_Name, table_has_fields.Field_Name, from_date, to_date)
        result = append(result, TableFieldCount{
            Table_Name: table_has_fields.Table_Name,
            Field_Name: table_has_fields.Field_Name,
            Count: table_count,
        })
    }
    return result
}


func getDateFilteredFieldCount(table_name string, date_filter_field string, from_date time.Time, to_date time.Time) int {
    db := mysqlConnection.GetConnection()
    var table_count int

    query := fmt.Sprintf(
        "SELECT count(*) FROM %s WHERE %s BETWEEN '%s' AND '%s'",
        table_name,
        date_filter_field,
        from_date.Format("2006-01-02 03:04:05"),
        to_date.Format("2006-01-02 03:04:05"),
    )    

    rows, err := db.Query(query)
    if err != nil {
        log.Fatal(err)
    }
    for rows.Next() {
        if err := rows.Scan(&table_count); err != nil {
            log.Fatal(err)
        }
    }
    return table_count
}
