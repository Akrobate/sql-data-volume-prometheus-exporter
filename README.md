# MySQL data prometheus exporter

This exporter will count number of lines of each table in database and will expose it.

## Grafana dashboard

A grafana dashboard can be found here to aggregate the data of this exporter
[Grafana dashboard](https://gitlab.com/Akrobate/grafana-dashboard-sql-data-volume-prometheus-exporter)


## Usage

First you need to configure env vars

### First method setting configuration.yml file

You can set the in configuration.yml copying configuration.example.yml to configuration.yml and setting vars

```yaml
exporter_port: 8083

mysql_port: 1234
mysql_host: my_host_var
mysql_name: my_db_name
mysql_user: my_db_user_name
mysql_password: my_db_password

refresh_interval_seconds: 3
refresh_fields_at_interval_seconds: 5
watch_date_fields: updated_at,deleted_at
```

### Second method setting env vars

```sh
EXPORTER_PORT=8083

MYSQL_PORT=1234
MYSQL_HOST=my_host_var
MYSQL_NAME=my_db_name
MYSQL_USER=my_db_user_name
MYSQL_PASSWORD=my_db_password

REFRESH_INTERVAL_SECONDS=3
REFRESH_FIELDS_AT_INTERVAL_SECONDS=5
WATCH_DATE_FIELDS=updated_at,deleted_at
```

## About watching fields with dates

This exporter can also count number of lines updated_at, or deleted_at with a count vector.
To configure fields you will need to list all fields that has to be watched in configuration watch_date_fields property.

For instance if your last updated field is named as updated_at, and last deleted field (date) is named deleted_at you can declare the in configuration as follows:

```yaml
watch_date_fields: updated_at,deleted_at
```


## Run exporter code

From root folder

```golang
go run .
```

## Run specific test

From test folder

```sh
cd genericDictionary
go test
```


## Build image

```sh
docker build -t sql-data-volume-prometheus-exporter .
```


## Run container

```sh
docker run --rm -p 8083:8083 -d -t sql-data-volume-prometheus-exporter
```



