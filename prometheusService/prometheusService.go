package prometheusService

import (
    "fmt"
    "github.com/prometheus/client_golang/prometheus"
    "sql-data-volume-prometheus-exporter/genericDictionary"
    "sql-data-volume-prometheus-exporter/configuration"
)


// Experimental parts

var Gauge_dict = genericDictionary.NewDictionary()


func getCounterVecParametrizedFieldName(field_name string) prometheus.Collector {
    counter_name := fmt.Sprintf("database_db_name_table_%s_count", field_name)
    counter_help := fmt.Sprintf("How many lines has been %s in table", field_name)
    return prometheus.NewCounterVec(
        prometheus.CounterOpts{
            Name: counter_name,
            Help: counter_help,
        },
        []string{
            "name",
        },
    )
}


func addCounterToDict(field_name string) {
    Gauge_dict.Set(field_name, getCounterVecParametrizedFieldName(field_name))
}
// Experimental parts // ENDS


var gauge_vector = prometheus.NewGaugeVec(
    prometheus.GaugeOpts{
        Name: "database_db_name_table_line_count",
        Help: "Number of rows per table, label names are tables names",
    },
    []string{
        "name",
    },
)

var prometheus_registry *prometheus.Registry

func init() {
    prometheus_registry = prometheus.NewRegistry()
    prometheus_registry.MustRegister(gauge_vector)

    configuration := configuration.GetInstance()
    fmt.Println(configuration.Watch_date_fields_list)
    for _, field_name := range configuration.Watch_date_fields_list {
        fmt.Println(field_name)
        addCounterToDict(field_name)
        prometheus_registry.MustRegister(Gauge_dict.Get(field_name).(prometheus.Collector))
    }
}

func GetRegistry() *prometheus.Registry {
    return prometheus_registry
}

func GetGaugeVector() *prometheus.GaugeVec {
    return gauge_vector
}
