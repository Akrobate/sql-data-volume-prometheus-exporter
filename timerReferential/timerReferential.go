package timerReferential

import (
    "time"
)


type TimerReferential struct {
    Lower_Boundary_Time time.Time
    Upper_Boundary_Time time.Time
}


func (tr *TimerReferential) UpdateTimes() {
    tr.Lower_Boundary_Time = tr.Upper_Boundary_Time
    tr.Upper_Boundary_Time = time.Now()
}


func NewTimerReferential() TimerReferential {
    return TimerReferential {time.Now(), time.Now()}
}
