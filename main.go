package main

import (
    "fmt"
    "net/http"
    "log"
    "time"
    "sql-data-volume-prometheus-exporter/configuration"
    "github.com/prometheus/client_golang/prometheus/promhttp"
    "sql-data-volume-prometheus-exporter/mysqlConnection"
    "sql-data-volume-prometheus-exporter/databaseCountService"
    "sql-data-volume-prometheus-exporter/prometheusService"
    "sql-data-volume-prometheus-exporter/timerReferential"
    "github.com/prometheus/client_golang/prometheus"
)


func main() {

    configuration := configuration.GetInstance()
    // fmt.Println(configuration)
    
    go func () {
        gauge_vector := prometheusService.GetGaugeVector()
        for _ = range time.Tick(time.Duration(configuration.Refresh_Interval_Seconds) * time.Second) {
            gauge_vector.Reset()
            for _, table_count := range databaseCountService.GetTableCounts() {
                gauge_vector.WithLabelValues(table_count.Name).Set(float64(table_count.Count))
            }
        }
    }()


    go func () {

        time_referential := timerReferential.NewTimerReferential()
        for _ = range time.Tick(time.Duration(configuration.Refresh_Fields_At_Interval_Seconds) * time.Second) {
            
            // fmt.Println(time_referential.Lower_Boundary_Time.Format("2006-01-02 03:04:05"))
            // fmt.Println(time_referential.Upper_Boundary_Time.Format("2006-01-02 03:04:05"))

            count_fields_data := databaseCountService.GetTableDateFieldAtCountList(
                time_referential.Lower_Boundary_Time,
                time_referential.Upper_Boundary_Time,
            )

            for _, field_data := range(count_fields_data) {
                
                // @todo Read about the interface to CounterVec necessary cast
                tmp_count_label_vect := prometheusService.Gauge_dict.Get(field_data.Field_Name).(*prometheus.CounterVec)
                
                tmp_count_label_vect.WithLabelValues(field_data.Table_Name).Add(
                    float64(field_data.Count),
                )
            }

            time_referential.UpdateTimes()
        }
    }()


    handler := promhttp.HandlerFor(prometheusService.GetRegistry(), promhttp.HandlerOpts{})
    http.Handle("/metrics", handler)

    if err := http.ListenAndServe(fmt.Sprintf(":%d", configuration.Exporter_Port), nil); err != nil {
        log.Fatal(err)
    }

    defer mysqlConnection.CloseCurrentInstance()

}
