package mysqlConnection

import (
    "fmt"
    "github.com/go-sql-driver/mysql"
    "sql-data-volume-prometheus-exporter/configuration"
    "log"
    "database/sql"
)


var is_connected bool = false
var mysql_connection_handler *sql.DB

func GetDatabaseCredentials() string {

    configuration := configuration.GetInstance()

    mysql_config := mysql.Config{
        User:   configuration.MySql_User,
        Passwd: configuration.MySql_Password,
        Net:    "tcp",
        Addr:   fmt.Sprintf("%s:%s", configuration.MySql_Host, configuration.MySql_Port),
        DBName: configuration.MySql_Name,
        AllowNativePasswords: true,
    }

    fmt.Println(mysql_config.FormatDSN())
    return mysql_config.FormatDSN()
}


func Open() *sql.DB {
	
	mysql_config_string := GetDatabaseCredentials()

	db, err := sql.Open("mysql", mysql_config_string)
	if err != nil {
		log.Fatal(err)
	}
	
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	return db
}


func GetConnection() *sql.DB {
	if (!is_connected) {
		mysql_connection_handler = Open()
		is_connected = true
	}
	return mysql_connection_handler
}


func Close(db *sql.DB) {
	db.Close()
}

func CloseCurrentInstance() {
	if (is_connected) {
		Close(mysql_connection_handler)
	}
}