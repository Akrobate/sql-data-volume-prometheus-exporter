package genericDictionary


type DictionaryElement struct {
	Name string
	Value interface{}
}


type Dictionary struct {
	Dictionary_list []DictionaryElement
}


func NewDictionary() Dictionary {
    return Dictionary{}
}


func (d *Dictionary) Set(name string, value interface{}) {
	index := d.findIndex(name)
	if (index < 0) {
		d.Dictionary_list = append(d.Dictionary_list, DictionaryElement{
			name,
			value,
		})
	} else {
		d.Dictionary_list[index].Value = value
	}
}


func (d *Dictionary) Unset(name string) {
	index := d.findIndex(name)
	if (index > 0) {
		d.Dictionary_list = append(d.Dictionary_list[:index], d.Dictionary_list[index+1:]...)
	}	
}


func (d *Dictionary) UnsetAll() {
	d.Dictionary_list = []DictionaryElement{}
}


func (d *Dictionary) Get(name string) interface{} {
	index := d.findIndex(name)
	if (index >= 0) {
		return d.Dictionary_list[index].Value
	}
	return nil
}


func (d *Dictionary) Keys() []string {
	var result []string
	for _, element := range d.Dictionary_list {
		result = append(result, element.Name)
	}
	return result
}


func (d *Dictionary) findIndex(name string) int {
	for index, element := range d.Dictionary_list {
		if (element.Name == name) {
			return index
		}
	}
	return -1
}

