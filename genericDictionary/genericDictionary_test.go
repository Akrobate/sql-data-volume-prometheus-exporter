// HELP
// From the genericDictionary run tests executing
// go test

package genericDictionary
 
import (
    "testing"
    "reflect"
)

func TestNewDictionary(t *testing.T) {
    dict := NewDictionary()
    expected := "genericDictionary.Dictionary"
    actual := reflect.TypeOf(dict).String()
    if expected != actual  {
        t.Errorf("Expected String(%s) is not same as"+
         " actual string (%s)", expected, actual)
    }
}


func TestAddElementAndRead(t *testing.T) {
    dict := NewDictionary()
    dict.Set("key", "TEST_VALUE")

    expected := "TEST_VALUE"
    actual := dict.Get("key")

    if expected != actual  {
        t.Errorf("Expected (%s) is not same as"+
         " actual (%s)", expected, actual)
    }
}


func TestUnexistingElementGet(t *testing.T) {
    dict := NewDictionary()
    if dict.Get("key") != nil  {
        t.Errorf("Expected unexisting key to return nil")
    }
}


func TestAddElementTwiceAndReadShouldReturnLastValue(t *testing.T) {
    dict := NewDictionary()
    dict.Set("key_1", "TEST_VALUE")
    dict.Set("key_1", "TEST_VALUE_UPDATED")

    expected := "TEST_VALUE_UPDATED"
    actual := dict.Get("key_1")

    if expected != actual  {
        t.Errorf("Expected (%s) is not same as"+
         " actual (%s)", expected, actual)
    }
}


func TestKeysCoherentSize(t *testing.T) {
    dict := NewDictionary()
    dict.Set("key_1", "TEST_VALUE_1")
    dict.Set("key_2", "TEST_VALUE_2")
    dict.Set("key_3", "TEST_VALUE_3")
    dict.Set("key_4", "TEST_VALUE_4")

    map_keys := dict.Keys()
    expected := 4
    actual := len(map_keys)

    if expected != actual  {
        t.Errorf("Expected (%d) is not same as"+
         " actual (%d)", expected, actual)
    }
}


func TestUnsetDictKey(t *testing.T) {
    dict := NewDictionary()
    dict.Set("key_1", "TEST_VALUE_1")
    dict.Set("key_2", "TEST_VALUE_2")
    dict.Set("key_3", "TEST_VALUE_3")
    dict.Set("key_4", "TEST_VALUE_4")


    if len(dict.Keys()) != 4  {
        t.Errorf("Key should be len of 4")
    }

    dict.Unset("key_2")

    if len(dict.Keys()) != 3  {
        t.Errorf("After key unsetted key len should be len of 3")
    }

    if dict.Get("key_2") != nil  {
        t.Errorf("Unsetted key should be nil")
    }

}


func TestUnsetAll(t *testing.T) {
    dict := NewDictionary()
    dict.Set("key_1", "TEST_VALUE_1")
    dict.Set("key_2", "TEST_VALUE_2")
    dict.Set("key_3", "TEST_VALUE_3")
    dict.Set("key_4", "TEST_VALUE_4")


    if len(dict.Keys()) != 4  {
        t.Errorf("Key should be len of 4")
    }

    dict.UnsetAll()

    if len(dict.Keys()) != 0  {
        t.Errorf("After UnsetAll len of Keys should be 0")
    }

}

